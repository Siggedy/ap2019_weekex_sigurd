var buttonPositionX = 66;
var buttonPositionY = 66;
var buttonWidth = 50;
var buttonHeight = 50;

var buttonColorR = 0;
var buttonColorG = 0;
var buttonColorB = 255;

var currency = 0;

var pressed = false;

function setup() {
  createCanvas(200,200);
  background(200,200,200);
}

function draw() {
  console.log("1:"+buttonColorG);
  fill(buttonColorR,buttonColorG,buttonColorB);
  console.log("2:" + buttonColorG);
  pressingButtons();
  rect(buttonPositionX,buttonPositionY,buttonWidth,buttonHeight);

  if (pressed == false) {
    buttonColorR = 0;
    buttonColorG = 0;
    buttonColorB = 255;
  }

  //mouseClicked();
  //console.log(counter);
  //console.log(pressed);
  //console.log(mouseX);
  //console.log(buttonPositionX);
  //console.log(mouseY);
}

function pressingButtons() {
  pressed = false;
  if (mouseIsPressed) {
    if (mouseX > buttonPositionX) {
        if (mouseX < buttonPositionX + buttonWidth) {
          if (mouseY > buttonPositionY) {
            if (mouseY < buttonPositionY + buttonHeight) { //This should place the clickable button where I want it
              console.log("Button pressed");
              buttonColorR = 10;
              buttonColorG = 200;
              buttonColorB = 255;
              pressed = true;
          }
        }
      }
    }
  }
}

/*function mouseClicked() {
  buttonColorR = 0;
  buttonColorG = 0;
  buttonColorB = 0;
  pressed = false;
  counter++;
}*/
