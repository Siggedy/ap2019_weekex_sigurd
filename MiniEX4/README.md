![Screenshot](butt_on.PNG)

RUNME

Why?
Buttons are traditionally a physical object, but as of the newer observation systems, i.e. Windows, Mac... Buttons have also become prevalent in the digital world.
However the digital world is incorporeal, meaning the tactile response that a user would relate a button to.
Luckily the digital world is also temporal, meaning it can be changed.
Therefore designers can have the button react to input, and create the illusion of a reaction, through an otherwise meaningless waste of processing power.
This reaction can create the illusion of a tactile response, such as a color-shift, a noise, a "shadow" that disappears, which also creates the illusion of a 3D space.

Whenever a user interacts with anything in a digital space, connected to the internet, there are many different companies that compete for access to the information
provided by the user interactions.
A button click becomes valuable. Because this information can tell companies where to place buttons, how they should look, and customer satisfaction.
This massive information well will be processed by complex algorithms, and processed into  readable and accesable information, but until then, the information is hidden,
and difficult to access.

These processes are shown in my miniEX, with a button reaction's colour change, and if you access the console, you'll see that there's a counter that increases
every time the button is clicked.