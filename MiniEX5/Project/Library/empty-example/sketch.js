var x = 0;
var amplitude = 30;
var lambda = 30*1.5-25;
var frequence = 30/40;
var y = 0;
var k = 0;

let aRectX = 30;
let fRectX = 30;
let lRectX = 30;

function setup() {
  createCanvas(1920,1080);
  background(255,255,255);
}

function draw() {
  createCanvas(1000,1000);
  background(255,255,255);
  drawSliders();
  translate(0,height/2)
  frameRate(60);
  waveMode0();
  console.log(mouseY);
}

function drawSliders() {
  /*aSlider = createSlider(amplitude);
  aSlider.position(20, 20);
  fSlider = createSlider(frequence);
  fSlider.position(20, 50);
  lSlider = createSlider(lambda);
  lSlider.position(20, 80);*/
  //Fuck it, custom slider time
  //Amplitude
  rect(20,27.5,200,5); //For the indication of slider size
  rect(aRectX-10, 20,15,20); //For the dragbox itself
  if (mouseIsPressed && mouseX > 0 && mouseX < 300 && mouseY > 0 && mouseY < 40) {
    aRectX = mouseX;
    if (aRectX > 220) {
      aRectX = 220;
    }
    if (aRectX < 19) {
      aRectX = 20
    }
  }
  amplitude = aRectX-19;
  //Frequence
  rect(20,67.5,200,5);
  rect(fRectX-10, 60,15,20);
  if (mouseIsPressed && mouseX > 0 && mouseX < 300 && mouseY > 59 && mouseY < 85) {
    fRectX = mouseX;
    if (fRectX > 221) {
      fRectX = 220;
    }
    if (fRectX < 19) {
      fRectX = 20
    }
    frequence = fRectX/40;
  }
  //lambda
  rect(20,107.5,200,5);
  rect(lRectX-10, 100,15,20);
  if (mouseIsPressed && mouseX > 0 && mouseX < 300 && mouseY > 99 && mouseY < 121) {
    lRectX = mouseX;
    if (lRectX > 221) {
      lRectX = 220;
    }
    if (lRectX < 19) {
      lRectX = 20
    }
    lambda = lRectX*1.5-25;
  }
}

function waveMode0() { //Doesn't work as intended, but it's pretty lit anywhoot It pastes the entire thing at once
//Remake, but make sure it places one dot at a time
  y = amplitude*cos(TAU*((x/lambda)-frequence*(millis()/1000)));
  k = TAU/lambda;

  for (let x = 0; x<width; x++) {
    point(x,amplitude*cos(TAU*((x/lambda)-frequence*(millis()/1000))))
    if (x==width) {
      x = 0;
    }
  }
}
/*
function waveMode1() {
  y = amplitude*cos(TAU*((x/lambda)-frequence*(millis()/1000)));
  k = TAU/lambda;

  if (x=>width) {
    point(x,amplitude*cos(TAU*((x/lambda)-frequence*(millis()/1000))))
  }
  if (x==width) {

  }
}
*/
