![Screenshot](404.PNG)
RUNME not available

I have made a drawing program, as a critique of the large amount of different smileys.
Represent. Relate. These are different sides of the same coin, in the discussion of multiple many emojis.
To represent in this context is simply to show something that has a relation to something else.
A relation on the other hand, is when a subject bears a likeness to another subject.
This likeness is often in physical properties, and in this emoji discussion

Communication is difficult with so many interpretable symbols, that each hold meaning.
The purpose of emojis is to convey feelings in a fast and easily interpretable way, to grant context to written text,
and it only works, because we can all agree on some general meaning. The more emojis, the more difficult communication becomes.
There can be many subtle interprative differences between using the homosexual family emoji, as contrasted by a heterosexual emoji,
and that difference can be interpreted in a massive multitude of ways. An example I'd like to highlight would be the "eggplant" emoji.
Does this mean we have to segregate people to use emojis that only they can relate to? Does it mean that emojis can only be used when the full context is known.
As of right now, it isn't a problem, and it might never be, but we must be vary of where the logical extreme leads.

The choice and the choice you don't make
One of existentialisms tenants is that humans are cursed with freedom, which roughly means, that whenever we make decisions or choices,
we get slightly worn out. Without being controversial in the least, I can claim, that making an active decision results in more cognitive load,
than making a passive decision, such as typing on a keyboard. The more choices a user has, and the more those choices resemble eachother,
the greater the cognitive load on the user, to distinguish the appropriate choice.
This can quickly become very uninclusive, as many people may feel a great pressure, when it comes to how they act in social situations,
this hypothetical, with an obscene amount of emojis has the same risk, at least for the uninitiated.

This is why I chose to make a drawing program, to envision a world where every emoji is possible, yet the solution is undesirable,
due to time consumption and unnecessary cognitive load.
I also created two different drawing types, and I wished to draw two identical looking emojis with different meanings.

A want for everyone to have something visual to identify with is unhealthy, if it is the identification that the person is fixated upon
An emoji is a representation of feelings on the inside, and not how you look on the outside, truly skin color shouldn't matter in this debate..