var colorBoxR = rect(10,0,10,255);
var colorBoxG = rect(30,0,10,255);
var colorBoxB = rect(50,0,10,255);

var colorCheckBox = rect(10,300,100,100);
var colorCheckBoxColor = fill(currentColorR, currentColorG, currentColorB);

var currentColorR = 0;
var currentColorG = 0;
var currentColorB = 0;

var activeColor = fill(0,0,0);

var drawType = 0;

var linepositionX = 560;
var linepositionY = 500;

function setup() {
  drawWorld();
}

function drawWorld() { //I can call this function to redraw the background
  createCanvas(1060,1000);
  background(255,255,255);
}

function draw() {
  push();
  colorBoxR; //Box to click to change color of drawing
  fill(255,0,0);
  pop();
  push();
  colorBoxG; //Box to click to change color of drawing
  fill(0,255,0);
  pop();
  push();
  colorBoxB; //Box to click to change color of drawing
  fill(0,0,255);
  pop();

  if (keyCode == 0) {
    drawType = 0; //Calling the drawWorld function
  }
  if (keyCode == 1) {
    drawType = 1;
  }

  if (mouseX < 9 && mouseX > 21,mouseY < -1 && mouseY > 256) { //click to change color RED

    if (mouseIsPressed) {
      currentColorR = mouseY;
      console.log("red");
    }
    //The further down you click, the more RED it is
  }
  if (mouseX < 29 && mouseX > 41,mouseY < -1 && mouseY > 256) { //click to change color GREEN

    if (mouseIsPressed) {
      currentColorG = mouseY;
      console.log("green");
    }
    //The further down you click, the more GREEN it is
  }
  if (mouseX < 49 && mouseX > 61,mouseY < -1 && mouseY > 256) { //click to change color BLUE

    if (mouseIsPressed) {
      currentColorB = mouseY;
      console.log("blue");
    }
    //The further down you click, the more blue it is
  }

  push(); //Make the box where you can see the color of your selected color combination
  colorCheckBox;
  colorCheckBoxColor;
  pop();
  if (drawType == 0) {
    if (mouseX < 60) {
      ellipse (mouseX,mouseY,10,10); //The size of the brush
      colorCheckBoxColor; //The color of the brush is selected
    }
  }

  if (drawType == 1) { //If you want to draw with an etch-a-sketch (different way of drawing)
    ellipse (linepositionX,linepositionY,3,3);
    if (keyCode == a) {
      linepositionX = linepositionX + 1;
    }
    if (keyCode == d) {
      linepositionX = linepositionX - 1;
    }
    if (keyCode == j) {
      linepositionY = linepositionY + 1;
    }
    if (keyCode == l) {
      linepositionY = linepositionY - 1;
    }
  }

  if (keyCode == BACKSPACE) {
    drawworld(); //Calling the drawWorld function. Meaning deleting everything and restarting
  }
}





//But I don't wanna just get rid of the first attempt (Trying to make a background)

/*var colorBoxR = rect(0,0,0,0);
var colorBoxG = rect(0,0,0,0);
var colorBoxB = rect(0,0,0,0);

var colorCheckBox = rect(0,0,0,0);
var colorCheckBoxColor = fill(currentColorR, currentColorG, currentColorB);

var currentColorR = 0;
var currentColorG = 0;
var currentColorB = 0;

var selectedColorDrawing = fill(0,0,0);

var backGroundR = 255;
var backGroundG = 255;
var backGroundB = 255;

var backgroundSelected = false;
var drawToolSelected = false;

function setup() {
  angleMode(DEGREES);
}

function drawWorld(){
  createCanvas(300,300);
  background(backGroundR,backGroundG,backGroundB);
  draw rect(1,1,1,1);//bgcolorselector
  draw rect(1,2,1,1);//drawtoolcolorselector
  draw rect(1,3,1,1);//checkMarkBox
}

function draw() {
  frameRate(60);
  if (section is pressed) {
    ellipse(1,1,mouseX,mouseY);
  }

  if (section is pressed) {
    //drawToolSelected = false;
    //popup 3 colorsquares
    //a colorchecker
    //a checkMark
    //If mouseX == < 9 but > 21
    //if (mouseIsPressed) RED
    //colorcheckbox changes corresponding to mouseY
    //If mouseX == < 29 but > 31
    //if (mouseIsPressed) GREEN
    //colorcheckbox changes corresponding to mouseY
    //If mouseX == < 49 but > 51 - betweens equal to variable deciding placement
    //if (mouseIsPressed) BLUE
    //colorcheckbox changes corresponding to mouseY
    //if flueben is mouseClicked
    //Backgroundcolorvariables become Backgroundcolorvariableschecker
    //backGroundR = currentColorR
    //backGroundG = currentColorG
    //backGroundB = currentColorB
    //currentColorR = 0;
    //currentColorG = 0;
    //currentColorB = 0;
    //colorcheckbox
    //backgroundSelected = false;
  }
  if (section is pressed) {
    //popup 3 colorsquares
    //a colorchecker
    //If mouseX == < 9 but > 21
    //if (mouseIsPressed) RED
    //colorcheckbox changes corresponding to mouseY
    //If mouseX == < 29 but > 31
    //if (mouseIsPressed) GREEN
    //colorcheckbox changes corresponding to mouseY
    //If mouseX == < 49 but > 51
    //if (mouseIsPressed) BLUE
    //colorcheckbox changes corresponding to mouseY
    //if flueben is mouseIsPressed
    //DrawingColorVariable
  }
  if (section is pressed) {
  //Small brush size (small circles) Can be made bigger (Choose own increments)
  //Slider, maybe
  }
  if (button backspace is pressed) {
    //all variables reset
    drawWorld
  }
  if (buttonClicked) {
    translate til send;

  }
}
*/
