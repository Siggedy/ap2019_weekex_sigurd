var x = 10;
var y = 25;
var r = 10;
var v = 0.47;
var tidsTaeller = 0;
var k = 2; //Korden er længden af en af siderne på trekanten
var længdeTaeller = 0;
var fallTime = 500;
var decelleration = 1;
var areWeToppling = Math.random();
var buenslaengde = 0; //Sådan beskriver man længden af cirklen med radius r
//Hvordan bevæger jeg den samtidig med at jeg roterer den. Dette er forbundet til tidsTaeller
var countDownSpin = 0;

var hasToppled = 0;
//Here starts ye olde DIY list til spring i 10%
var toppling = 0;


function setup()
{
  createCanvas(2000,2000);
  background(200,150,100); //Tilfældig baggrund hjælper mig med at sikre at det virker (hvilket det ikke gør)

  frameRate (60); //I control the framerate
  fallTime = 500; //Det her er
  angleMode(degrees);

  k = 2*r*sin(60/2); //korden er længden af den ligesidede trekants sider
  buenslaengde = 120/180*TAU/2*r; //Sådan beskriver man længden af halvcirklen med radius r og grader 120 (60+60)
}

function drawWorld()
{
  background(200,150,100); //Now I only have to write out drawWorld() when I wannt make my background, reducing lines from 2 to 1
  triangle(0,25,750,525,0,525);
}

function draw()
{
  drawWorld();
  tidsTaeller = tidsTaeller +1; //I base movement on time only
  console.log(tidsTaeller);

  if (tidsTaeller < 500) //Det tager 500 for tidsTaeller at når bunden
  {
    push();
    translate (0.0030*tidsTaeller*tidsTaeller,0.0020*tidsTaeller*tidsTaeller); //Bevæger sig ned ad bakken
    triangle(x+r*sin(v),y+r*cos(v),x+r*sin(v+TAU/3),y+r*cos(v+TAU/3),x+r*sin(v+2*TAU/3),y+r*cos(v+2*TAU/3)) //En ligesidet trekant
    fill(255); //Farven
    stroke(0,0,0); //Omridset
    pop();


    længdeTaeller = længdeTaeller + 0.5*k; //Tæller opad. PROBLEM, uafhængig af distancen, men afhængig af tid
    console.log(k);
    if (længdeTaeller => k)//længden af en af siderne. Når længdetælleren er lig sidelængden
    {
      //hasToppled+20% toppling = 1;x
      if (længdeTaeller => k)
      {
        if (toppling => 0); //Hvis den skal til at vælte
        {
          if (areWeToppling => 40)
          {
            push();
            rotate(120); //Skal gøres flydende
            //v=v-0.000003
            //translate(sin(buenslaengde), cos(buenslaengde))
            //Den vælter
            //, den laver en bevægelse fra centrum over en cirkel
            //Den skal slet ikke rotere, men bevæge sig i en halvcirkel med radius lig r (Så burde den vende sig om det nederste trekants koordinat)
            countDownSpin = countDownSpin - 1; //Tæller medens den drejer
            pop();
            if (countDownSpin == 0) //Når den har udført handlingen ovenover i nok tid, skal alle værdiger genstartes
            {
              areWeToppling
              toppling = 0;
              hasToppled = hasToppled + 10;
              areWeToppling = 0;
            }
          }
          else
          {
            areWeToppling = 0;
            hasToppled = 0;
            areWeToppling = hasToppled+areWeToppling*100; //hasToppled adds a probability to areWeToppling
          }
        }
        længdeTaeller = 0;
      }

    }
  }
  else
  {
    if (fallTime < 597)
    {
      decelleration = decelleration - 0.005;
    }
    fallTime = fallTime + decelleration;
    push();
    translate (0.003*fallTime*fallTime,0.002*tidsTaeller*tidsTaeller); //x Skal gøres igen, men omvendt. Den springer en lille værdi ud af x
    console.log(fallTime);
    translate (0,0)
    triangle(x+r*sin(v),y+r*cos(v),x+r*sin(v+TAU/3),y+r*cos(v+TAU/3),x+r*sin(v+2*TAU/3),y+r*cos(v+2*TAU/3))
    fill(255);
    stroke(0,0,0);
    //translate(sin(0.003)*(tidsTaeller*tidsTaeller), cos(0.003)*(tidsTaeller*tidsTaeller))
    v = v - 0.05;//Custom rotation
    pop();
    //Prøv at beskriv i en eksponentiel kurve
  }
}