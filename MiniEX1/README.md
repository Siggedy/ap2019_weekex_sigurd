![Screenshot](Trille trille.PNG)

The Code is here
https://cdn.staticaly.com/gl/Siggedy/ap2019_weekex_sigurd/raw/master/MiniEX1/WEEK1/Project/Library/empty-example/index.html

RUNME


My original intent was to make a triangle, that slid down another triangle
This sliding effect would have to accelerate, sometimes it should randomly topple over
Further it should fall off the triangle, when it reached that point.
In the end I had everything but the toppling effect working

The reason the toppling didn't work, is due to the way I expressed movement,
and some math I couldn't work out in time.
Maybe if I'd based my expression of movement on a parabola, or if I'd reconstructed the factors we have on earth
i.e Forces, gravity and resistance based on position/contact
Or if I'd define movement based on distance